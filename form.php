<?php
$gender = array(0 => 'Nam', 1 => 'Nữ');
$faculty = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
$info = array();
$info["name"] =  "<label class='empty'></label><br>";
$info["gender"] =  "<label class='empty'></label><br>";
$info["department"] =  "<label class='empty'></label><br>";
$info["bday"] =  "<label class='empty'></label><br>";
$info["address"] =  "<label class='empty'></label><br>";

if (isset($_POST['submit'])) {

    session_start();
    $_SESSION["name"] = "";
    $_SESSION["gender"] = "";
    $_SESSION["department"] = $faculty[$_POST["department"]];
    $_SESSION["bday"] = "";
    $_SESSION["address"] = "";


    if (empty($_POST["name"]))
        $info["name"] = "<label class='error'>Hãy nhập tên!</label><br>";
    else
        $_SESSION["name"] = $_POST["name"];

    if (!isset($_POST['gender']))
        $info["gender"] = "<label class='error'>Hãy chọn giới tính</label><br>";
    else
        $_SESSION["gender"] = $_POST['gender'];

    if (empty($_POST["bday"]))
        $info["bday"] = "<label class='error'>Hãy chọn ngày sinh!</label><br>";
    else
        $_SESSION["bday"] = $_POST["bday"];

    if ($_SESSION["department"] === "")
        $info["department"] = "<label class='error'>Hãy chọn phân khoa!</label><br>";

    if ($_SESSION["name"] != ""  && $_SESSION["gender"] != "" && $_SESSION["bday"] != "" && $_SESSION["address"] != "")
        header("Location: cf.php");
}
echo
"<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link rel='stylesheet' href='form.css'>
    <title>form</title>
</head>

<body>
    <fieldset>
        <form method='post' action='cf.php' enctype='multipart/form-data'>";
echo $info["name"];
echo $info["gender"];
echo $info["department"];
echo $info["bday"];
echo $info["address"];
echo "
            <table>
                <tr >
                    <td class='td'><label>Họ và tên*</label></td>
                    <td><input type='text' id ='input' class = 'bbox' name ='name' value='";
echo isset($_POST['name']) ? $_POST['name'] : "";
echo "'></td>
                </tr>
                <tr>
                    <td class = 'td'><label>Giới tính*</label> </td>
                    <td>";
for ($i = 0; $i < count($gender); $i++) {
    echo ' <input type="radio" name="gender" value="' . $i . '"';
    echo (isset($_POST['gender']) && $_POST['gender'] == $i) ? "checked " : "";
    echo "/>" . $gender[$i];
}
echo
"</td>
                </tr>
                <tr>
                    <td class='td'>
                        <label>  Phân khoa* </label>    </td>
                    <td ><select class = 'bbox' name = 'department'>";
foreach ($faculty as $key => $value) {
    echo "<option";
    echo (isset($_POST['department']) && $_POST['department'] == $key) ? " selected " : "";
    echo " value='" . $key . "'>" . $value . "</option>";
}
echo "
                        </select></td>
                </tr> 
                <tr>
                    <td class='td'><label>Ngày sinh* </label></td>
                    <td><input type='date' class='bbox' name='bday' value='";
echo isset($_POST['bday']) ? $_POST['bday'] : "";
echo "'></td>
                </tr> 
                <tr >
                    <td class ='td'><label>Địa chỉ </label></td>
                    <td><input type='text' id ='input' class = 'bbox' name ='address' value='";
echo isset($_POST['address']) ? $_POST['address'] : "";
echo "'></td>
                </tr> 
                <tr >
                    <td class ='td'><label>Hình ảnh </label></td>
                    <td><input type='file' id ='image' name ='image' accept='image/png, image/jpeg' style='width: 220px'></td>

                </tr>                
            </table>
            <button name='submit' type='submit'>Đăng ký</button>
        </form>
    </fieldset>
</body>";
