<?php
session_start();
if (isset($_FILES['image'])) {
    $aExtraInfo = getimagesize($_FILES['image']['tmp_name']);
    $imgUrl = "data:" . $aExtraInfo["mime"] . ";base64," . base64_encode(file_get_contents($_FILES['image']['tmp_name']));;
};
echo
"<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <link rel='stylesheet' href='cf.css'>
    <title>do_regist</title>
</head>

<body>
    <fieldset>
        <form>
            <table>
                <tr>
                    <td class='td'><label>Họ và tên</label></td>
                    <td><label>" . $_SESSION["name"] . "</label></td>
                </tr>
                <tr>
                    <td class='td'><label>Giới tính</label></td>
                    <td><label>" . $_SESSION["gender"] . "</label></td>
                </tr>
                <tr>
                    <td class='td'><label>Phân khoa</label></td>
                    <td><label>" . $_SESSION["department"] . "</label></td>
                </tr>
                <tr>
                    <td class='td'><label>Tuổi</label></td>
                    <td><label>" . $_SESSION["age"] . "</label></td>
                </tr>
                <tr>
                    <td class='td'><label>Hình ảnh</label></td>
                    <td><img src='$imgUrl' alt='Image' />
                </td>
                </tr>
            </table>
        </form>
    </fieldset>

</body>";
